[[_TOC_]]

/assign me @bsall @Juahyio
/label FAQ::to-confirm
/milestone %Documentation
<!-- /confidential -->

# Context
<!-- Explain what this new documentation page will solve. You can include issue links to related ~Support issues. -->


# Suggested knowledge base article 
<!-- You can start writing the knowledge base below but 
the full text will need to be sent as a new page in the 
/src/docs/FAQ/ directory, through a new Merge Request  -->

```markdown
# <title of the knowledge-base page>
[[_TOC_]] 
## Problem
<!-- Describe the problem observed, maybe including error or logs extracts to confirm the FAQ case this is about.-->


## Cause
<!-- If the cause to this issue is know, you can add such paragraph. For example : external dependency failure, outdated version, ... -->

## Resolutions
### Workaround
 <!-- remove if not relevant-->
<!-- if workardound(s) are known, it can is helpful to document it  for users to try -->
<!-- ### workaround 1 : ... -->


### Solution
<!-- If a solution is known, step by step how to resolve the problem -->

<!-- Always leave the following section in all pages-->
## Ask for help
[FAQ/Ask-for-help](https://gitlab.com/nomadic-labs/umami-wallet/umami/-/wikis/FAQ/Ask-for-help)
```

<!--  Process : when this issue is validated (with the tag FAQ::todo) please proceed and   -->
<!--  create the FAQ following the process :                                                -->
<!--  https://gitlab.com/nomadic-labs/umami-wallet/umami/-/wikis/FAQ/How-to-create-a-new-FAQ -->
<!-- /confidential # avoid setting the issue as confidential unless you have a private fork available for MR-->