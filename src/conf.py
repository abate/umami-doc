# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'doc.umami.io'
copyright = '2023, umami team'
author = 'umami team'
release = '0.0.1'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinxcontrib.mermaid",
    "myst_parser"
]

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output
myst_heading_anchors = 4
pygments_style = 'sphinx'

html_logo = 'img/logo.png'
html_theme = 'sphinx_rtd_theme'
#html_static_path = ['_static']

html_css_files = [
    'css/custom.css',
]

html_js_files = [
    'https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.17/d3.min.js',
]
html_domain_indices = False


