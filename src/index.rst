Welcome to Umami Wallet documentation!
==================================================


The Project
-----------

This website
------------

This website (https://doc.umamiwallet.com) provides online user oriented documentation,
although it also provides an high level pointers regarding the umami wallet.

The documentation is automatically generated from the master branch of the https://gitlab.com/nomadic-labs/umami-wallet/umami-doc/.
To propose a patch, please clone the repository and submit a merge request or follow the process described in docs/contrib/FAQ decisions

The technical documentation currently resides in the gitlab wiki: https://gitlab.com/nomadic-labs/umami-wallet/umami/-/wikis/home


.. toctree::
   :maxdepth: 1
   :caption: User Documentation

   docs/doc/How-to-install-Umami
   docs/doc/Android-F-Froid-Umami-repo
   docs/doc/Batch-File-Format-Specifications
   docs/doc/Manage-Accounts
   docs/doc/Managing-Networks
   docs/process/terminology

.. toctree::
   :maxdepth: 1
   :caption: Support Knowledge Base

   docs/FAQ/index
   docs/FAQ/Ask-for-help
   docs/FAQ/Error-429-Too-many-Requests
   docs/FAQ/How-to-connect-Umami-to-DApps
   docs/FAQ/How-to-update-Umami

.. toctree::
   :maxdepth: 1
   :caption: Contribution guidelines

   docs/contrib/Contributing
   docs/contrib/FAQ decisions


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
