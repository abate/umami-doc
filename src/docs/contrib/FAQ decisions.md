# Contributing to umami-doc
[[_TOC_]]
## Context
Thanks for your interest in the Umami-wallet documentation. 
Feel free to suggest any documentation you might find helpful !

We describe here the how to contribute to this documentation project.

## Process for new documentation page

### Quick links 

 * Create new issue from template: [Create new FAQ](https://gitlab.com/nomadic-labs/umami-wallet/umami-doc/-/issues/new?&issuable_template=[Doc]%20Knowledge%20Base)
 * Validation board (limited access): [FAQ board](https://gitlab.com/groups/nomadic-labs/umami-wallet/-/boards/5333569)

### Sequence Diagram

```mermaid
sequenceDiagram
    actor sya as Creator

    participant gi as Gitlab Issue
    participant gm as Gitlab Merge Request
    actor u as Validator
    Note over sya: Doc Idea

    sya ->>+ gi : Create new Issue using template
    activate gi
    gi --) u    : Issue created with proper labels and assignee
    
    Note over u : Make sure to check board and todos for notification

    alt Refine idea
       u ->> gi : comments on issue
      sya ->> gi: comments on issue
    else Issue Validated
       u ->> gi   : Validates the issue 

       gi ->>+ gm : Create linked Merge Request from the Issue as `draft:`
       activate gm 

       loop Create or edit files 
         sya --) gm : push edits
         gm --) sya : view in test environment from MR
       end

       sya ->> gm : Mark Merge Request as Ready

       gm ->> u : assigned as mandatory reviews (CODEOWNERS)
       Note over u : Make sure to check board and todos for notification
       u ->> gm : approve Merge Request
       u ->> gm : Maintainer merges Merge Request       
       gm --) gi : (merge request closes issue)
       deactivate gm
    else Suggestion not validated
       u ->> gi : closes the issue explaining why
    end
    deactivate gi
```


### validation workflows

* Issue Validation goes with the following labels :
  * ~FAQ::to-confirm : set by default from template
  * => ~FAQ::todo : set by Umami Team 
  * => ~FAQ::done or issue closed : either when the Merge Request is merged or the issue is refused

* Merge Request process :
  * "Create Merge Request" from the Issue, marking it as `Draft:` _not ready_
  * Add your contribution in the branch created along the merge-request
    * Usually: modifying a page or adding a page in `/src/docs/FAQ/` directory
    * Make sure that you page is correctly formated
    * When the commit is created on the branch, a CI/CD pipeline is created from the Merge Request 
    * IF the pipeline suceeds, a link is created to a temporary environment where you can see your modifications (link "View app"). 
  * When the contributions are over and you're happy with the result from the "View App" temporary website, make the Merge Resquest as "ready" (removes the `draft:` from the title)
  * Umami Team should then pick the Merge Request and publish it, closing the issue


Errors handling :
* If an issue arise with the CI, this means that there is an issue either with your modification or with the CI code. Either fix the issue or contact Umami Team.
* 


## Git workflow

Branching example form the `master` with rebase to the temporary branch `mr-idea` created along with the merge request

```mermaid
gitGraph
   commit
   commit
   branch mr-faq-idea
   checkout master
   commit
   commit
   checkout mr-faq-idea
   commit
   commit
   merge master
   commit
   checkout master
   merge mr-faq-idea
   commit
```
