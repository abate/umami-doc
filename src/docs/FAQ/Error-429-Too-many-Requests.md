# [FAQ] Error 429: Too many Request
[[_TOC_]] 

## Problem
When loading the balance, Umami does not load the balance of accounts, with an error `Http error response (429) Too Many Requests`

<!-- versions : 0.5.5 and before -->

## Diagnosis
Account(s) balance not showing and Umami Logs showing `Http error response (429) Too Many Requests` error message.

 ![image](/img/Error-429-Too-many-Requests/bc6a8259-2734-4728-a886-6f30b55f4552-image.png)

## Cause

Umami relies on tezos nodes to load the balances and connects to a tezos public nodes to do so.
The list of known public nodes is statistic and embedded in Umami. 
One node randomly from this list when starting the Umami application.

## Workaround

### Option 1 : restart umami
Quit the umami application and restart it. You have a 4/5 chance to select another node where your rate limit hasn't been reached.

### Option 2 : force reload
Umami is based on ElectronJS, you can hence force the reload using the chrome shortcut:
- Linux / Windows : `Ctrl + R`
- MacOs : `Cmd + R`

### Option 3 : personal node
If you have a running octez tezos node, you can go in Umami settings and use it.
