# How to update Umami
[[_TOC_]]

## :desktop: Desktop

* **Step 1** : Go to Settings 
![image](/img/How-to-update-Umami/9cd84612-58cd-41c1-937b-939995b9957e-image.png)

* **Step 2** : Scroll to the bottom of the page and click “check for updates” 
![image](/img/How-to-update-Umami/f150c781-3c2c-4757-98d1-3d58bd83c859-image.png)

* **Step 3** : A download button will appear should there be an available update, click on the download button 
![image](/img/How-to-update-Umami/01b0df87-ee22-4acc-9bf9-261d0344db11-image.png)

* **Step 4** : Once the download is done, click on “install and restart now” to update your Desktop App to the latest version available. 
![image](/img/How-to-update-Umami/a7d26dbd-af1c-4f3a-a0f0-a467b5ba5ac9-image.png)

* **Step 5** : The latest Version of Umami Desktop will now be installing, wait for the installation to finish and for the app to launch by itself. 
![image](/img/How-to-update-Umami/66ac8d04-ea6b-4540-96f6-527415a2593e-image.png)

## :iphone:  Mobile
**IOS**
* **Step 1** : Open the App Store
* **Step 2** : Search for Umami Wallet
* **Step 3** : Click the Update button

**Android**
* **Step 1** : Open the Google Play
* **Step 2** : Search for Umami Wallet
* **Step 3** : Click the Update button

_Please Note:_
To protect your keys to the wallet/accounts, the application does suggest keeping the mnemonic (a.k.a. the recovery phrase--a.k.a. the 24 words), which is shown to the user when creating a wallet.

So, as long as you have your recovery phrase with you, you will be able to (re-)load your accounts on Umami (or even other wallets), even if all other data is lost (e.g. if your device becomes inoperable, etc.). However, Please keep in mind that you should not be sharing it with anyone. If someone else gets access to your mnemonic... they could also gain access to your accounts... so please keep the mnemonic secure.

If you want to learn more on the subject of keys/accounts or other Umami related topics, we have a Medium channel with several articles: https://medium.com/umamiwallet
