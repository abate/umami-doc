[[_TOC_]]

# Ask for help

There are various ways to ask for help with the  Umami Wallet for Tezos :
 DEMO !!!!

## :e-mail: E-mail
 Ask for help by e-mail to the umami support address [umami-support@nomadic-labs.com](mailto:umami-support@nomadic-labs.com)
<!-- [support address](mailto:incoming+nomadic-labs-umami-wallet-umami-20392089-issue-@incoming.gitlab.com) -->

## :open_file_folder: File a gitlab issue
 Open a support issue directly on gitlab : [open an support issue](https://gitlab.com/nomadic-labs/umami-wallet/umami/-/issues/new?issuable_template=support)


## :speech_balloon: Slack channel on `tezos-dev`
 Ask a question on `#umami` channel on tezos-dev slack: [#umami on tezos-dev](https://tezos-dev.slack.com/archives/C01TT28QR18) 



## Info and announcements

* Twitter https://twitter.com/UmamiWallet
* Medium https://medium.com/umamiwallet
* Public wiki (here) https://gitlab.com/nomadic-labs/umami-wallet/umami/-/wikis/home
* Website https://www.umamiwallet.com