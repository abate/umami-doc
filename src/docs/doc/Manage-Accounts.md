# Managing Accounts In Umami

## Desktop 🖥️

During your first installation of Umami desktop or following an offboarding, you will have to select one of the following options:

- [Create a New Secret]
- [Import a Secret with a Recovery Phrase]
- [Restore from Backup File]
- [Connect a Ledger Device]
- [Sign up or Sign in with Google]

You will be guided through each process if you select it.

![KB_-_Desktop_-_Options_first_onboarding](/img/Manage-Accounts/f1f3bcf6-542d-49cb-8541-ccdc2987dbfe-KB_-_Desktop_-_Options_first_onboarding.png)

You will also be able to create or import additional secrets and accounts at any other point:

1. In the side navigation bar, click the **Accounts** tab.
2. At the top right corner of the screen, click **Management View**.
3. Click **Create or Import Secret**.
![KB_-_Desktop_-_Create_or_import_secret](/img/Manage-Accounts/63663eca-240d-4ac9-b582-da0ed06e33c5-KB_-_Desktop_-_Create_or_import_secret.png)
4. Select the desired option.
![KB_-_Desktop_-_Onboarding_choice](/img/Manage-Accounts/3ed98f85-1590-44df-9462-95287282541d-KB_-_Desktop_-_Onboarding_choice.png)

----

## Mobile 📱

During your first installation of Umami mobile or if you have offboarded Umami mobile previously, you will have to select one of the following options:

1. [Create a New Secret]
2. [Import a Secret with a Recovery Phrase]
3. [Import a Secret into Umami Mobile]

![mobile_-_onboarding](/img/Manage-Accounts/695b966a-8ec6-4afd-bab4-36296135c8d8-mobile_-_onboarding.jpg)

  


## Create a New Secret

### Desktop 🖥️

1. Click **Create New Secret**.
2. Record your 24-words recovery phrase.
![KB_-_Desktop_-_Recovery_phrase_censored](/img/Manage-Accounts/2caa1fe3-e4e9-4308-a78a-9f3473124c72-KB_-_Desktop_-_Recovery_phrase_censored.png)
3. Verify that you have properly recorded your recovery phrase by entering the words corresponding to each sequence number.
![KB_-_Desktop_-_Create_new_secret_-_Verify](/img/Manage-Accounts/18187b01-bf40-4f80-a1a3-19060657b737-KB_-_Desktop_-_Create_new_secret_-_Verify.png)
4. Specify a [custom derivation path] or leave as it is and click **Continue**.
![KB_-_Desktop_-_Create_new_secret_-_Derivation_path](/img/Manage-Accounts/3b567c97-6e85-445e-9579-9cc3d3373bf3-KB_-_Desktop_-_Create_new_secret_-_Derivation_path.png)
5. Optional. Select a storage location for a backup file. You can either click **Browse for folder** or enter the folder’s pathname. Click **Continue**. You may also skip this step by clicking **Set up later in settings**. You will be able to create a backup file later on in the Settings tab.
![KB_-_Desktop_-_Create_new_secret_-_Storage_location](/img/Manage-Accounts/d8568e29-8adf-4644-bc60-cb549b419500-KB_-_Desktop_-_Create_new_secret_-_Storage_location.png)
6. Set a password to secure your wallet.
![KB_-_Desktop_-_Create_new_secret_-_Password](/img/Manage-Accounts/07048d02-8324-4f7d-85e4-1d1adf24c5bc-KB_-_Desktop_-_Create_new_secret_-_Password.png)

----

### Mobile 📱

1. Tap on **Create New Secret**.
2. Record your 24-words recovery phrase.
![mobile_-_record_reco_phrase](/img/Manage-Accounts/47509d92-33fa-4bd2-981e-9c7e33af9110-mobile_-_record_reco_phrase.jpg)
3. Verify that you have properly recorded your recovery phrase by selecting the word corresponding to each sequence number.
![mobile_-_reco_phra_check_x4](/img/Manage-Accounts/9c0f4457-506f-4ade-aec2-1e41ecd28ead-mobile_-_reco_phra_check_x4.jpg)
4. Set a password to secure your wallet and decide if you want to use biometric authentication with your wallet by switching the toggle on/off.
![mobile_-_set_password](/img/Manage-Accounts/724348bf-f46b-47e4-b35b-83bd05856319-mobile_-_set_password.jpg)

  


## Import a Secret with a Recovery Phrase 🖥️

Umami support various recovery phrase lengths: 12, 15, 18, 21 or 24 words. This enables users to import secrets from other Tezos wallets.

### Desktop 🖥️

1. Click **Import Secret with Recovery Phrase**.
2. In the entry form, select the correct recovery phrase format.
![KB_-_Desktop_-_Import_secret_-_Recovery_phrase_format](/img/Manage-Accounts/2079fe89-8ccd-4d4c-982f-0e8d71badff0-KB_-_Desktop_-_Import_secret_-_Recovery_phrase_format.png)
3. Enter the words that compose the recovery phrase.
4. Optional. Click **Customize Derivation Path** if you want to specify a [custom derivation path].
5. Set a password that will protect your wallet.

----

### Mobile 📱

1. Tap on **Import Secret with Recovery Phrase**.
2. Enter the words that compose the recovery phrase and tap on **Continue**.
![mobile_-_import_secret_-_reco_phrase](/img/Manage-Accounts/23b5436d-8ece-4042-b2c1-6e4ec348e26a-mobile_-_import_secret_-_reco_phrase.jpg)
3. Set a password to secure your wallet and decide if you want to use biometric authentication with your wallet by switching the toggle on/off.

  


## Restore from Backup File 🖥️

This option is available only during your first installation of Umami or if you have offboarded Umami previously. Umami also supports backup files from Galleon.
1. Click **Restore from Backup**.
2. Select your backup file. You can either click **Upload File** or enter the file’s pathname.
3. Enter the password used to encrypt the file.
![KB_-_Desktop_-_Restore_from_backup_1](/img/Manage-Accounts/cf965501-ce48-4a9c-b7e2-6001b2ad8bb4-KB_-_Desktop_-_Restore_from_backup_1.png)
4. Click **Access wallet**. 
  
 Umami wallet opens with your keys displayed.

  


## Connect a Ledger Device 🖥️

> **Before you Start**
> - [ ] Plug the Ledger into the computer using a USB cable.
> - [ ] Unlock your Ledger.
> - [ ] Make sure your Ledger has the latest firmware version.
> - [ ] Install and open the Tezos Wallet app on your Ledger.
  


1. In Umami desktop, click **Connect Ledger**.
2. Click the **Export Public Key** button.
3. On the Ledger device, confirm the public key export.
5. In Umami desktop, a popup displaying the list of accounts opens.
![KB_-_Desktop_-_Ledger_-_Accounts_list](/img/Manage-Accounts/a0848683-c29a-4639-9ad9-45e041dae0fb-KB_-_Desktop_-_Ledger_-_Accounts_list.png)
6. Optional. Click **Advanced Options** if you want to [select a derivation scheme] or specify a [custom derivation path]. Then click **Verify Accounts** and check the accounts you are about to import.
7. Click **Import**.

  


## Sign up or Sign in with your Google account 🖥️

This method will load a seedless account. As such, there is no recovery phrase associated to this account.

1. Click the **Google** button.
2. Choose the Google account you want to use.
3. Allow Umami to access your Google account.

  


## Show Recovery Phrase

### Desktop 🖥️

1. In the side navigation bar, click the **Accounts** tab.
2. At the top right corner of the screen, click **Management View**.
3. Next to a secret, on the right, click the **More** button. In the menu, click **Show**.
![KB_-_Desktop_-_Reveal_1_3](/img/Manage-Accounts/5c1b8c81-9f25-4b84-803f-050f870f7e8a-KB_-_Desktop_-_Reveal_1_3.png)
4. Enter your password to reveal your recovery phrase.   

Your recovery phrase is displayed in a popup window.

----

### Mobile 📱

1. Tap the **Settings** icon at the top right corner of the screen.
![mobile_-_settings_icon](/img/Manage-Accounts/a1587c51-7b47-4cd1-a05e-50f19d2df456-mobile_-_settings_icon.jpg)
2. Select **Show recovery phrase**.
![mobile_-_settings_list_reco_phra](/img/Manage-Accounts/9c4fe21c-6754-4533-a711-5447b36aa217-mobile_-_settings_list_reco_phra.jpg)
3. Authenticate to reveal the recovery phrase.

  


## Import a Secret into Umami Mobile 🖥️ 

This option is available only during your first installation of Umami mobile or if you have offboarded Umami mobile previously.

1. In Umami desktop, in the side navigation bar, click the **Accounts** tab.
2. At the top right corner of the screen, click **Management View**.
3. Next to a secret, on the right, click the **Export** button.
![KB_-_Desktop_-_Export_to_mobile_1](/img/Manage-Accounts/5f8cd76d-cf2f-4ae0-9395-eb23f24b7fbc-KB_-_Desktop_-_Export_to_mobile_1.png)
  
 A popup displaying a QR code opens.
![KB_-_Desktop_-_Export_to_mobile_2](/img/Manage-Accounts/47e7b94c-6470-45b2-b5a8-09aa79097c6d-KB_-_Desktop_-_Export_to_mobile_2.png)
4. Open Umami wallet on your mobile and tap **Import Secret** with Umami desktop.
5. Tap **Scan QR** and scan the QR code displayed in Umami desktop. 
  
 Umami mobile opens with your secret imported.

  


## Create a Backup File 🖥️

With a backup file generated in Umami, you will only be able to restore your accounts on another installation of Umami. Galleon backup files are also supported. Please note that the backup file can only contain secrets that have a recovery phrase.

1. In the side navigation bar, at the bottom, click the **Settings** icon button.
2. In the Settings tab, find the **Wallet Backup** section near the bottom of the page.
3. Switch the **toggle** on and select the file’s path. You can either click **Browse for folder** or enter the folder’s pathname. Click **Save**.
![KB_-_Desktop_-_Backup_file_1](/img/Manage-Accounts/dbe2f0fd-163a-4bb6-938a-1fb063f1a65c-KB_-_Desktop_-_Backup_file_1.png)
![KB_-_Desktop_-_Backup_file_2](/img/Manage-Accounts/8db2ef98-75b2-4e84-b8c1-64491db16296-KB_-_Desktop_-_Backup_file_2.png)
![KB_-_Desktop_-_Backup_file_3](/img/Manage-Accounts/f6b5fc12-bd4e-40db-a946-4b118f8adb61-KB_-_Desktop_-_Backup_file_3.png)
4. Click **Save**.

Please note that the backup file is a password-protected. If you want to restore your wallet from a backup, you will need both the backup file and the password used to encrypt it.

  


## Derive an Account from a Secret

### Desktop 🖥️

1. In the side navigation bar, click the **Accounts** tab.
2. At the top right corner of the screen, click **Management View**.
3. Next to a secret, on the right, click the **Add Account** button.
![KB_-_Desktop_-_Add_account](/img/Manage-Accounts/a9541e09-1482-4905-8bd8-2e681ccc788f-KB_-_Desktop_-_Add_account.png)
4. Enter a name for the account and click **Add**.
5. Enter your password and click **Confirm**.

----

### Mobile 📱

1. Tap on the **Swap** icon button at the top right corner of the screen.
![mobile_-_add_account_-_swap_icon](/img/Manage-Accounts/a2bc957c-0505-4bc6-9230-5f24a6656891-mobile_-_add_account_-_swap_icon.jpg)
2. Tap on the **Add** icon button at the top right corner of the screen.
![mobile_-_add_account_-_add_icon](/img/Manage-Accounts/bd7da155-8ca4-4325-9278-f947981762a9-mobile_-_add_account_-_add_icon.jpg)
3. Choose a name for the account you are about to create.
4. Authenticate to reveal the recovery phrase.

  


## Remove a Secret or an Account 🖥️

1. In the side navigation bar, click the **Accounts** tab.
2. At the top right corner of the screen, click **Management View**.
3. Next to a secret or a Google account, on the right, click the More button. In the menu, click **Delete**.
![KB_-_Desktop_-_Delete](/img/Manage-Accounts/f462f20a-b2a8-4a59-8e41-c0d49debf3a2-KB_-_Desktop_-_Delete.png)
4. Confirm and click **Delete**.

  


## Scan a Secret 🖥️

If an account is not displayed in your wallet, you can run a scan of the secret. This will retrieve all the derived accounts active on the blockchain.

1. In the side navigation bar, click the **Accounts** tab.
2. At the top right corner of the screen, click **Management View**.
3. Next to a secret, on the right, click the **More** button. In the menu, click **Scan**.
4. Enter your password and click **Confirm**.
5. A list of active derived accounts is displayed, click **Import** to add them to your wallet.
![KB_-_Desktop_-_Scan_-_List](/img/Manage-Accounts/2293985e-4f91-4bf6-83c5-83ad5ccb3313-KB_-_Desktop_-_Scan_-_List.png)

  


## Edit a Secret 🖥️

1. In the side navigation bar, click the **Accounts** tab.
2. At the top right corner of the screen, click **Management View**.
3. Next to a secret, on the right, click the **More** button. In the menu, click **Edit**.
![KB_-_Desktop_-_Edit_secret](/img/Manage-Accounts/8c0ce3a2-7df7-43bf-9546-27bf3c0d60f8-KB_-_Desktop_-_Edit_secret.png)
4. Enter the new name of the secret.
5. Click **Update**.

  


## Edit an Account 

### Desktop 🖥️

1. In the side navigation bar, click the **Accounts** tab.
2. At the top right corner of the screen, click **Management View**.
3. Next to an account, on the right, click the **Pencil** button.
![KB_-_Desktop_-_Edit_account](/img/Manage-Accounts/01d2c1e5-ea88-423f-87cd-39bdeb211400-KB_-_Desktop_-_Edit_account.png)
4. Enter the new name of the account.
5. Click **Update**.

----

### Mobile 📱

1. Next to an account, on the right, tap on the **Swap** icon button.
![mobile_-_add_account_-_swap_icon](/img/Manage-Accounts/a84792fd-4cd4-4cbf-bf41-efacdc00cd03-mobile_-_add_account_-_swap_icon.jpg)
2. Next to an account, on the right, tap on the **Pencil** icon button.
![mobile_-_edit_account_-_edit_icon](/img/Manage-Accounts/07520243-a1d6-41c1-9d7b-e1e35c607700-mobile_-_edit_account_-_edit_icon.jpg)
3. Enter the new name of the account and tap **Save**.

  


## Set a Custom Derivation Path 🖥️

This is an advanced feature available during the creation and the importation of a secret. This feature allows users to access different account addresses and create new addresses. 
 
By default, Umami uses the following path `m/44'/1729'/?'/0'` and the default key. This path is slightly different from the [BIP-44 specification](https://github.com/bitcoin/bips/blob/master/bip-0044.mediawiki). The wildcard `?'` stands for the account index, which is incremented as Umami scans for a secret’s derived accounts as well as whenever a user creates a new address in Umami.
 
The derivation path is made up of 5 levels and indexes, each with a special meaning:

| Level | Index | Meaning |
|---|---|---|
| 1 | `m` | Refers to the master key. |
| 2 | `purpose'` | A constant set to `44'`. |
| 3 | `coin_type'` | A constant set for each blockchain. For Tezos, the number is `1729'`. |
| 4 | `account'` | Splits the key space into independent user identities, so the wallet never mixes the coins across different accounts. Users can use these accounts to organize the funds in the same fashion as bank accounts; for saving purposes, for company purposes, for common expenses, etc.   
 Accounts are numbered from index 0 in sequentially increasing manner. |
| 5 | `address_index'` | Addresses are numbered from index 0 in sequentially increasing manner.   
 This number is used as a child index in [BIP32 derivation](https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki). |

  


***Steps***

1. As you are importing a secret, click **Customize Derivation Path**.
2. Enter a custom derivation path. Please note that Umami will not accept paths that do not begin with `m/44'/1729'`. Here are examples of derivation paths and the keys derived in Umami.

| Level | Index |
|---|---|
| `m/44'/1729'/?'/0'` | All of the revealed addresses with a positive balance (`m/44'/1729'/0'/0'`, then `m/44'/1729'/1'/0'` , `m/44'/1729'/2'/0'` and so on) |
| `m/44'/1729'/0'/0'` | First address |
| `m/44'/1729'/1'/0'` | Second address |
| `m/44'/1729'/1'/1'` | ⚠️ None - Invalid path in Umami |
| `m/44'/1729'/2'/0'` | Third address |

  


## Set a Derivation Scheme 🖥️

This is an advanced feature available only if you connect a Ledger device to Umami wallet. The derivation scheme (or elliptic curve) doesn’t need to be changed. This feature concerns the creation of new addresses. 

Umami supports 3 derivation schemes. Depending on the one you choose, the new implicit account addresses you create will start with either `tz1`, `tz2`, or `tz3`.

| Derivation Scheme | Address Prefix |
|---|---|
|Ed25519|`tz1`|
|Secp256k1|`tz2`|
|P-256|`tz3`|

  


***Steps***

1. As you are connecting a Ledger device to Umami, click **Advanced Options** to select a derivation scheme or specify a custom derivation path.
![KB_-_Desktop_-_Ledger_-_Advanced_options](/img/Manage-Accounts/a903842a-b9d3-4c52-b4a9-62f537116109-KB_-_Desktop_-_Ledger_-_Advanced_options.png)
![KB_-_Desktop_-_Ledger_-_Advanced_options__menu_open_](/img/Manage-Accounts/768eecc3-4afb-4aa4-b543-9b9ee6ed47a7-KB_-_Desktop_-_Ledger_-_Advanced_options__menu_open_.png)
2. Click **Verify Accounts** and check the accounts you are about to import.
3. Click **Import**.

  


## Offboard Wallet

Offboarding will permanently delete any data from your computer or mobile. 

Your accounts are still available to be imported in the future. 

⚠️ In order to regain access to your accounts, please make sure that you keep the recovery phrase.

### Desktop 🖥️

1. In the side navigation bar, at the bottom, click the **Settings** icon button.
2. In the Settings tab, find the **Danger Zone** section at the bottom of the page.
3. Click on **Offboard**.
4. Acknowledge that you have read and understood the disclaimer. Click **Confirm**.
![KB_-_Desktop_-_Offboard_wallet](/img/Manage-Accounts/5e5b8a03-90fc-40df-95a7-f48558032684-KB_-_Desktop_-_Offboard_wallet.png)

----

### Mobile 📱

1. Tap the **Settings** icon at the top right corner of the screen. 
2. Select **Offboard Wallet**.
3. Acknowledge that you have read and understood the disclaimer. Tap **Confirm**.
