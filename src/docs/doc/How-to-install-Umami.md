# How to install Umami

## Desktop

1. Visit [umamiwallet.com](https://umamiwallet.com/)
2. Click **Download** in the top menu bar.
![Screenshot_2023-01-05_at_12.47.21](/img/How-to-install-Umami/72263f89-e590-4a65-85d0-a19a679decf6-Screenshot_2023-01-05_at_12.47.21.png)

3. Make sure the correct OS is displayed and click **Download now**. You can also select the right OS from the options below.   

![Screenshot_2023-01-05_at_12.37.49](/img/How-to-install-Umami/97abd3af-3c66-40e8-b534-6e19852733ad-Screenshot_2023-01-05_at_12.37.49.png)

4. For macOS users, a pop up will appear, click **Open**.   

![popup.svg](/img/How-to-install-Umami/d83178fd-8d61-4653-bb7a-6ad172c4b2ea-popup.svg)


  


## Mobile  

### iOS and Android
1. Visit [umamiwallet.com](https://umamiwallet.com/)
2. Click on **Download**.
3. A QR code will appear, tap **click HERE** below and you will be routed to your official app store.   

![clickhere3.svg](/img/How-to-install-Umami/a75369e2-9b53-43f0-a09d-b0446820cc94-clickhere3.svg)
4. From here, complete the usual app store download confirmation process.


### F-droid
1. Either scan the QR code on [this page](https://fdroid.umamiwallet.com/repo/?fingerprint=82EC3725EC7D8F54F740E5DA9E8CF66BBE4ECACC2FA6B7B41AE5B6FE29F754EF) or copy [this link](https://fdroid.umamiwallet.com/repo/?fingerprint=82EC3725EC7D8F54F740E5DA9E8CF66BBE4ECACC2FA6B7B41AE5B6FE29F754EF). 
2. Open F-Droid, go to Settings/Repositories and add a repository. 
Both Repository address and Fingerprint fields should be automatically filled from your clipboard.  

The fingerprint (SHA-256) of the repository signing key is:
`82 EC 37 25 EC 7D 8F 54 F7 40 E5 DA 9E 8C F6 6B BE 4E CA CC 2F A6 B7 B4 1A E5 B6 FE 29 F7 54 EF`


