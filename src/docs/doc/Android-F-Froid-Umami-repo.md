# Getting Umami on F-Droid

Copy [this url](https://fdroid.umamiwallet.com/repo?fingerprint=82EC3725EC7D8F54F740E5DA9E8CF66BBE4ECACC2FA6B7B41AE5B6FE29F754EF) and paste it in F-Droid

## Context
F-Droid is an android marketplace for open-source projects.

Umami mobile is not (yet) built by F-Droid currently, but you still can install Umami Mobile on android through the F-Droid application by adding our specific repository, where our latest Android builds are pushed

<!-- This procedure can also be used to make sure that you are always on the latest published version of Umami through F-Droid -->

<!-- We advice against installing multiple versions of Umami through different marketplaces : either install it directly by hand with the APK, F-droid official repo, Frdroid Umami repo, google playstore. --> 


## Step by step

1. Follow instructions at https://fdroid.umamiwallet.com/repo/  
   or  
   go to `Settings / Repositories / + button` and fill the entries manually:
   * URL:  `https://fdroid.umamiwallet.com/repo`
   * Fingerprint: `82EC3725EC7D8F54F740E5DA9E8CF66BBE4ECACC2FA6B7B41AE5B6FE29F754EF`
4. Navigate out of the settings, and search for `Umami Mobile`. If it does not show up, refresh the packages list (swipe down).
