# Managing Networks
[[_TOC_]]

## Add a custom network on your desktop

1. Go to Settings > Chain/Network and click 'Add custom network'
2. Fill out the form
![Capture_d_écran_2023-01-03_à_12.11.03](/img/Managing-Networks/9d9ed073-0ae4-4797-817c-70949d096a6d-Capture_d_ecran_2023-01-03_12.11.03.png)
While you can use the node of your choice (see list below), we recommend that you use `https://mainnet.umamiwallet.com` as Mezos URL.  

Here are a few public nodes Umami usually connects to on mainnet:

   - `https://mainnet.smartpy.io/`
   - `https://mainnet-tezos.giganode.io`
   - `https://api.tez.ie/rpc/mainnet/`
   - `https://rpc.tzbeta.net/`
   - `https://teznode.letzbake.com`
   - `https://mainnet.tezrpc.me/`

## Switch networks on your desktop
1. Go to Settings > Chain/Network 
2. Click a network to select it out of the options available. The network change will happen immediately.

## Remove a custom network on your desktop
1. Go to Settings > Chain/Network and click the bin icon button.
![Capture_d_écran_2023-01-03_à_13.23.47](/img/Managing-Networks/9d09668f-76f8-437e-b1b2-ed83feeb503e-Capture_d_ecran_2023-01-03_13.23.47.png)
2. Click ‘Delete’
![Capture_d_écran_2023-01-03_à_13.04.28](/img/Managing-Networks/05ff3ebc-5aac-4c49-bfa0-14579c478655-Capture_d_ecran_2023-01-03_13.04.28.png)
