Umami documentation

# install python3

    sudo apt-get install python3-venv

# install poetry

    curl -sSL https://install.python-poetry.org | python3 -

# install all the project's dependencies

    poetry install

# Build the documentation website

    make -C src html
